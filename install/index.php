<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class project_form extends CModule {

    public $MODULE_ID = 'project.form';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_FORM_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_FORM_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_FORM_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        $eventManager = EventManager::getInstance();
        Loader::includeModule($this->MODULE_ID);

        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('form', 'onBeforeResultAdd', $this->MODULE_ID, '\Project\Form\Event\Form', 'onBeforeResultAdd');
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);

        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('form', 'onBeforeResultAdd', $this->MODULE_ID, '\Project\Form\Event\Form', 'onBeforeResultAdd');

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    protected function GetConnection() {
        return Application::getInstance()->getConnection(FavoritesTable::getConnectionName());
    }

}
