<?

namespace Project\Form\Event;

use CFile,
    Project\Core\Form\Config;

class Form {

    static public function onBeforeResultAdd($WEB_FORM_ID, &$arFields, &$arrVALUES) {
        if ($WEB_FORM_ID == Config::FEEDBACK) {
            if (!empty($arrVALUES['form_file_36']) and is_numeric($arrVALUES['form_file_36'])) {
                $arrVALUES['form_file_36'] = CFile::MakeFileArray($arrVALUES['form_file_36']);
            }
        }
    }

}
